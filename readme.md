# Como pillar info del Arduino

Hay 4 formas de coger información del Arduino:
- Distancia: enviar "0" (0x30) por el puerto serie.
- Acelerómetro: enviar "1" (0x31) por el puerto serie.
- Giroscopio: enviar "2" (0x32) por el puerto serie.
- Magnetómetro: enviar "3" (0x33) por el puerto serie.